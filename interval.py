"""
Author: Aleck MacNally
Purpose:
    A Class to represent intervals on the Real line
    which allows the user to specify the inclusion
    of the bounds.
"""

import re

# Inheriting from object makes the code python2/3 compliant
#pylint: disable=useless-object-inheritance

class Interval(object):

    """ A class for representing an interval on a continuous
        numerical set. The empty interval is represented as:
        Interval('(0,0)').
    """

    def __init__(self, str_rep):
        pattern = r"^\s*(?P<left>[\[\(])\s*(?P<lbound>(\-?\d+\.?\d*)|\-?inf)?\s*\," + \
                  r"\s*(?P<ubound>(\-?\d+\.?\d*)|\-?inf)?\s*(?P<right>[\]\)])\s*$"
        matches = re.match(pattern, str_rep)

        self.left = matches.group('left') == '['
        self.right = matches.group('right') == ']'

        try:
            self.lbound = float(matches.group('lbound'))
        except TypeError:
            if self.left:
                raise ValueError("The bracket must be open for an infinite bound.")
            self.lbound = float('-inf')
        try:
            self.ubound = float(matches.group('ubound'))
        except TypeError:
            if self.right:
                raise ValueError("The bracket must be open for an infinite bound.")
            self.ubound = float('inf')

        if self.lbound > self.ubound:
            raise ValueError('Lower bound value must be less that Upper bound value')
        if self.lbound == self.ubound:
            if self.left != self.right:
                raise ValueError('If the interval consists of one or fewer points' \
                        + ' both brackets must be of the same type.')
            if self.left == self.right == False:
                self.lbound = self.ubound = 0

    def __str__(self):
        return Interval.display_interval(self.left, self.lbound, self.ubound, self.right)

    def __repr__(self):
        return "Interval('%s')" % str(self)

    def __contains__(self, num):
        below = num < self.lbound if self.left else num <= self.lbound
        above = num > self.ubound if self.right else num >= self.ubound
        return (not below) and (not above)

    def __add__(self, inter):
        lbound = self.lbound + inter.lbound
        ubound = self.ubound + inter.ubound
        left = self.left and inter.left
        right = self.right and inter.right

        return Interval(Interval.display_interval(left, lbound, ubound, right))

    def __sub__(self, inter):
        lbound = self.lbound - inter.ubound
        ubound = self.ubound - inter.lbound
        left = self.left and inter.left
        right = self.right and inter.right

        if lbound > ubound:
            lbound, ubound = ubound, lbound

        return Interval(Interval.display_interval(left, lbound, ubound, right))

    def __mul__(self, inter):
        multi_list = [(self.lbound * inter.lbound, self.left and inter.left),
                      (self.lbound * inter.ubound, self.left and inter.right),
                      (self.ubound * inter.ubound, self.right and inter.right),
                      (self.ubound * inter.lbound, self.right and inter.left)]
        lbound, left = min(multi_list, key=lambda x: x[0])
        ubound, right = max(multi_list, key=lambda x: x[0])
        print(Interval.display_interval(left,lbound,ubound,right))

        return Interval(Interval.display_interval(left, lbound, ubound, right))

    #TODO: Fix, whats wrong with mul/truediv
    # a = [1.0,)
    # b = (,1.0)
    # b / a -> error
    def __truediv__(self, inter):
        if 0 not in inter:
            if inter.ubound != 0 and inter.lbound != 0:
                inverse_inter = Interval(Interval.display_interval(inter.right,
                    1.0 / inter.ubound, 1.0 / inter.lbound, inter.left))
                print(1)
            elif inter.lbound != 0:
                inverse_inter = Interval(Interval.display_interval(False,
                    '-inf', 1.0 / inter.lbound, inter.left))
                print(2)
            else:
                inverse_inter = Interval(Interval.display_interval(inter.right,
                    1.0 / inter.ubound, 'inf', False))
                print(3)
        else:
            inverse_inter = Interval(Interval.display_interval(False,
                '-inf', 'inf', False))
            print(4)

        print(inverse_inter)

        return self * inverse_inter

    @staticmethod
    def set_subtract(inter0, inter):
        """ Set subtract inter from inter0.
        """

        if Interval.are_disjoint(inter0, inter):
            return {inter0}

        resultant_set = set()
        if inter.ubound in inter0:
            if inter.lbound in inter0:
                lower = Interval(Interval.display_interval(inter0.left, inter0.lbound,
                    inter.lbound, not inter.left))
                upper = Interval(Interval.display_interval(not inter.right, inter.ubound,
                    inter0.ubound, inter0.right))
                resultant_set.add(lower)
                resultant_set.add(upper)
            else:
                upper = Interval(Interval.display_interval(not inter.right, inter.ubound,
                    inter0.ubound, inter0.right))
                resultant_set.add(upper)
        else:
            if inter.lbound in inter0:
                lower = Interval(Interval.display_interval(inter0.left, inter0.lbound,
                    inter.lbound, not inter.left))
                resultant_set.add(lower)

        return resultant_set


    @staticmethod
    def are_disjoint(inter1, inter2):
        """ Returns True if inter1 and inter2 are disjoint intervals.
        """
        return inter1.ubound < inter2.lbound or \
                inter2.ubound < inter1.lbound or \
                (inter1.ubound == inter2.lbound and not (inter1.right and inter2.left)) or \
                (inter2.ubound == inter1.lbound and not (inter2.right and inter1.left))

    @staticmethod
    def simplify_union(*inters):
        """ Returns the union of n intervals.
        """

        union = set(inters)
        overlap = True

        while overlap:
            for i in union:
                restart = False
                for j in union - {i}:
                    if not Interval.are_disjoint(i, j):
                        unel = i.union(j)
                        union.remove(i)
                        union.remove(j)
                        union.add(unel.pop())
                        restart = True
                        break
                if restart:
                    break
                else:
                    overlap = False

        return union

    @staticmethod
    def simplify_intersect(*inters):
        """ Returns the union of n intervals.
        """

        intersect = set(inters)

        while len(intersect) > 1:
            for i in intersect:
                for j in intersect - {i}:
                    ijint = i.intersect(j)
                    intersect.remove(i)
                    intersect.remove(j)
                    intersect.add(ijint)
                    restart = True
                    break
                if restart:
                    break
                else:
                    return Interval('(0,0)')


        return intersect.pop()

    def intersect(self, inter):
        """ Represents the intersection of the interval
            with the inter.
        """
        if Interval.are_disjoint(self, inter):
            return Interval('(0,0)')

        ubound = min(self.ubound, inter.ubound)
        lbound = max(self.lbound, inter.lbound)

        if self.lbound > inter.lbound:
            left = self.left
        elif self.lbound < inter.lbound:
            left = inter.left
        else:
            left = (self.left and inter.left)

        if self.ubound > inter.ubound:
            right = inter.right
        elif self.ubound < inter.ubound:
            right = self.right
        else:
            right = (self.right and inter.right)

        if ubound == lbound:
            left = right = left and right

        return Interval(Interval.display_interval(left, lbound, ubound, right))

    def union(self, inter):
        """ Represents the union of the interval with
            inter.
        """

        if Interval.are_disjoint(self, inter):
            return {self, inter}

        if self.lbound > inter.lbound:
            left = inter.left
        elif self.lbound < inter.lbound:
            left = self.left
        else:
            left = self.left or inter.left

        if self.ubound > inter.ubound:
            right = self.right
        elif self.ubound < inter.ubound:
            right = inter.right
        else:
            right = self.right or inter.right

        lbound = min(self.lbound, inter.lbound)
        ubound = max(self.ubound, inter.ubound)

        if ubound == lbound:
            left = right = left or right

        return {Interval(Interval.display_interval(left, lbound, ubound, right))}

    @staticmethod
    def display_interval(left, num1, num2, right):
        """ Returns a string representing the interval provided
            in the arguments.
        """

        lbrack = "[" if left else "("
        rbrack = "]" if right  else ")"

        # for num in (num1, num2):
            # num = num if num == float('-inf') or num == float('inf') else ""

        return "{}{}, {}{}".format(lbrack, num1, num2, rbrack)

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, inter):
        return self.lbound == inter.lbound and \
               self.ubound == inter.ubound and \
               self.left == inter.left and \
               self.right == inter.right

    def __lt__(self, inter):
        if self.lbound == inter.lbound:
            return self.left and not inter.left
        return self.lbound < inter.lbound

    def __le__(self, inter):
        return (self == inter) or (self < inter)


    def __gt__(self, inter):
        if self.ubound == inter.ubound:
            return self.right and not inter.right
        return self.ubound > inter.ubound

    def __ge__(self, inter):
        return (self == inter) or (self > inter)
