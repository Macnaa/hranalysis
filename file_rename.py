"""
Author: Aleck MacNally
Purpose:Rearrange folder structure
(not generalizable)
"""
import os
import shutil


def folder_analysis(folder):
    """ Analyses folder structure and returns a dictionary
        of dates->(players, file idx) and a set of players.
    """

    subfolders = [dI for dI in os.listdir(folder) if os.path.isdir(os.path.join(folder, dI))]

    dates = {}
    players = set()
    for sf in subfolders:

        players.add(sf)

        for f in os.listdir(os.path.join(folder, sf)):

            dt = f[:6]

            try:
                dates[dt].append((sf, f[6:]))
            except KeyError:
                dates[dt] = [(sf, f[6:])]

    return dates, players

def folder_rearrange(folder, new_folder, structure):
    """ Rearranges the folder to the given structure
        which is a tuple of dates->(players, file idx)
        and player set.
    """
    dates, players = structure

    for date in dates.keys():

        for (player, filekey) in dates[date]:

            from_name = os.path.join(folder, player, date + filekey)
            to_name = os.path.join(new_folder, date, player + "_" + date + "_" + filekey)
            # print("%s -> %s" % (from_name, to_name), flush=True)
            os.makedirs(os.path.dirname(to_name), exist_ok=True)
            shutil.copy2(from_name, to_name)

    # for player in players:
        # player = os.path.join(folder, player)
        # os.rmdir(player)


def folder_file_rename(folder):
    """ Function which copies folder tree and
        then rearranges the original folder
        with a new predefined structure.
    """

    # shutil.copytree(folder, folder + "_copy")
    new_folder = folder + "_copy"

    structure = folder_analysis(folder)
    folder_rearrange(folder, new_folder, structure)
    return new_folder, structure
