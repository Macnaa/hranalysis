"""
Author: Aleck MacNally
Purpose: Take a folder arranged as per
briefing and rearrange it then perform heart
rate analysis on each resultant file.
"""

import os
import argparse

import file_rename
import heart_rate_analysis

def argument_parser():
    """
    Function: wraps argument parser
    Return Parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("folder",
                        help="Folder in which all files are stored.")
    parser.add_argument("-e", "--epoch", type=int, default=15,
                        help="Epoch legth in seconds.")
    parser.add_argument("-v", "--verbose", action='store_true',
                        help="Verbose output")
    return parser

def folder_processing(folder, epoch, players_HRs_dict):
    """ Function for calling HeartRateAnalysis Class on each of the
        rearranged files.
    """
    subfolders = [dI for dI in os.listdir(folder) if os.path.isdir(os.path.join(folder, dI))]

    for date in subfolders:
        date_file = list()

        for f in os.listdir(os.path.join(folder, date)):

            player, date, filekey = f.split("_")
            hrmax, hrrest = players_HRs_dict[player]
            to_name = os.path.join(folder, date, player + "_" + date + "_" + filekey + ".dat")
            from_name = os.path.join(folder, date, f)

            file_analysis_object = heart_rate_analysis.HeartRateAnalysis(from_name, epoch, hrmax, hrrest)
            file_string = str(file_analysis_object)

def get_heart_rates(player):
    return (200, 40)

def main():
    """ Entry point.
    """

    parser = argument_parser()
    args = parser.parse_args()
    folder = args.folder
    epoch = args.epoch

    if folder[-1] == "/" or folder[-1] == "\\":
        folder = folder[:-1]

    new_folder, (dates, players) = file_rename.folder_file_rename(folder)

    players_HRs_dict = {}
    for player in players:
        players_HRs_dict[player] = get_heart_rates(player)

    folder_processing(new_folder, epoch, players_HRs_dict)

if __name__ == "__main__":
    main()




