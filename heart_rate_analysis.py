"""
Author: Aleck MacNally
Purpose: Class for analyzing heart-rate files.
"""

import datetime
import statistics as st

import numpy as np

from interval import Interval

class HeartRateAnalysis:

    """ Class for the various operations included
        in processing a Heart Rate File.
    """

    _zones = list(zip(list(np.linspace(0,1,21))[:-2],
        list(map(lambda x: x+0.05, list(np.linspace(0,1,21))))[:-2]))
    HR_ZONES = list(map(Interval, ['[' + str(x) + ", " + str(y) + ')' for x, y in _zones])) \
            + [Interval('[0.95,1.0]')]

    @staticmethod
    def isinzone(zone, n):
        """ Checks if a value is with an interval.
        """
        lbrack, lbound, ubound, rbrack = zone
        below = n <= lbound if lbrack else n < lbound
        above = n >= ubound if rbrack else n > ubound
        return (not below) and (not above)

    def __str__(self):
        pass

    def __repr__(self):
        pass

    def __init__(self, fn, epoch, hrmax, hrrest, start=0, end=-1):
        """ Constuctor function which populates the analysis fields.
        """

        self._epoch = epoch
        self._start = start
        self._end = end
        self._hrv = list()

        with open(fn, "r") as file_ob:
            self._hrv = self._file_process(file_ob)

        self._av_hr = st.mean(bpm for _, _, bpm in self._hrv)
        self._obs_start = self._hrv[0][0]
        self._obs_end = self._hrv[-1][0]
        self._obs_totalt = self._obs_end - self._obs_start

        hrmax_fn = lambda hr : hr / float(hrmax)
        hrreserve_fn = lambda hr: (hr - hrrest) / (float(hrmax) - float(hrrest))

        self._hrmax_zones = self._time_in_zones(HeartRateAnalysis.HR_ZONES, hrmax_fn)
        self._hrreserve_zones = self._time_in_zones(HeartRateAnalysis.HR_ZONES, hrreserve_fn)

        self._percentiles = dict()
        percentiles_of_interest = list(range(0,101,5))
        for p in percentiles_of_interest:
            self._percentiles[p] = np.percentile(np.array([bpm for _, _, bpm in self._hrv]), p)

        self._et_value = self._edwards_trimp(hrmax_fn)
        breakpoint()



    def _time_in_zones(self, zones, ref):
        """ Private fn: Calculates the time spent
            in a particulat heart rate zone, caluclated
            with the function `ref`.
        """

        def _time_in_zone(zone, hr_list, ref):
            """ Private fn: Calculates the time in
                a zone.
            """

            zone_time = datetime.timedelta()

            for stt, ft, bpm in hr_list:
                percent = ref(bpm)
                # if HeartRateAnalysis.isinzone(zone, percent):
                if percent in zone:
                    zone_time += ft - stt
            return zone_time

        zone_dict = dict()

        for zone in zones:
            zone_dict[zone] = _time_in_zone(zone, self._hrv, ref)

        return zone_dict

    def _file_process(self, file_ob):
        """ Private fn: Processes a file object to produce
            a epoch smoothed heart rate vector.
        """

        date_line = file_ob.readline()
        player_line = file_ob.readline()
        _ = file_ob.readline()

        self.date, self.time_simple, self.activity = date_line.split(" - ")
        self.player = player_line.split(" ")[0]

        for idx, _ in enumerate(file_ob):
            if idx >= self._start - 1:
                return self._epoch_process(file_ob)
        return list()


    def _epoch_process(self, file_ob):
        """ Processes a hear rate vector into epochs.
        """

        epoch_length = datetime.timedelta(seconds=self._epoch)

        hr_list = list()

        timen_1 = None
        current_time = datetime.timedelta()
        epoch_hr_list = list()
        epoch_starttime = None
        max_bpm = 0

        for idx, line in enumerate(file_ob):
            if self._end != -1 and idx >= self._end:
                break
            if not line or line == '\n':
                continue

            t_str, beats = line.split('\t')
            delta_t = datetime.timedelta()

            t = datetime.datetime.strptime(t_str, "%H:%M:%S.%f")

            if timen_1 is None:
                timen_1 = t
                epoch_starttime = t
            else:
                delta_t = t - timen_1
                timen_1 = t

            current_time += delta_t

            if current_time > epoch_length:

                epoch_median = st.median(epoch_hr_list)

                if epoch_median > max_bpm:
                    max_bpm = epoch_median

                hr_list.append( (epoch_starttime, t, epoch_median) )

                current_time = datetime.timedelta()
                epoch_starttime = t
                epoch_hr_list = list()

                continue

            bpm = (1 / float(beats))*60000
            epoch_hr_list.append(bpm)

        # if epoch_hr_list:
            # hr_list.append( (epoch_starttime, t, st.median(epoch_hr_list)) )

        return hr_list

    def _edwards_trimp(self, hrmax_fn):
        """ Edwards TRIMP Function
        """
        zones = [Interval('[50, 60)'), Interval('[60, 70)'),
                 Interval('[70,80)'), Interval('[80, 90)'), Interval('[90,100]')]
        zone_time = self._time_in_zones(zones, hrmax_fn)

        return zone_time[zones[0]] + 2*zone_time[zones[1]] + \
                3*zone_time[zones[2]] + 4*zone_time[zones[3]] + 5*zone_time[zones[4]]

    def _lucia_gore_trimp(self, hrmax_fn):
        """ Lucia Gore TRIMP Function
        """
        zones = [Interval('[60, 75)'), Interval('[75, 90)'), Interval('[90, 100]')]
        zone_time = self._time_in_zones(zones, hrmax_fn)

        return zone_time[zones[0]] + 2*zone_time[zones[1]] + 3*zone_time[zones[2]]

    def _bannisters_trimp(self, hrreserve_fn):
        """ Bannisters TRIMP Function
        """
        return hrreserve_fn(self._av_hr) * self._obs_totalt * 0.64 * np.exp(hrreserve_fn(self._av_hr))



